#And then there were microservices and the developer did not know how much could go wrong

We have often heard about chaos monkeys and circuit breakers. What does this mean in practise however. Consider the 8 falacies of distributed computing. 

### https://en.wikipedia.org/wiki/Fallacies_of_distributed_computing 

Fallacies_of_distributed_computing
- The network is reliable
- Latency is zero
- Bandwidth is infinite
- The network is secure
- Topology doesn't change
- There is one administrator
- Transport cost is zero
- The network is homogeneous

Ask yourself - 

What will happen to your service if the network stops working for a minute or if the network is saturated and latency spikes?
What will happen to your service if an upstream service you rely on is backed up? Are you going to keep passing requests through to it when it cannot even deal with the requests it is currently facing?

`A Circuit breaker  is used to detect failures and encapsulates the logic of preventing a failure from constantly recurring, during maintenance, temporary external system failure or unexpected system difficulties.`

And even if you introduce a circuit breaker, how are you testing that your service correctly resumes after the circuit has reset itself?

Distributed testing is hard!


### The goal of this lab is to show you how you can test your system for upstream services failing or slowing down.

Istio provides the ability developer to simulate complex network conditions directly onto the cluster. It achieves this through the use of the sidecar Envoy proxy that is deployed with each service.

A naive test could be expressed like this
```javascript

const request = require('request-promise-native')
const util = require('util');
const exec = util.promisify(require('child_process').exec);

describe('view cache service',  () => {

  
    //
    test('should load empty object if back end service is unreachable', async (done) => {
        const { stdout, stderr } =  await exec('echo $PATH')

        await exec('kubectl apply -f ./integration-tests/back-end-gateway-with-fault-injected.yaml')
        const promise = request({timeout:1000, uri:'http://integration.myservice.com/get-data'})
        promise.then(function(value) {
            expect(false).toBeTruthy()
            done()
        }).catch(function(value) {
            expect(true).toBeTruthy()
            done()
            
        })

    })
})

afterEach(async () => {
    await exec('kubectl apply -f ../back-end/back-end-gateway.yaml')
});

``` 
Here we are applying a Istio config against my cluster to introduce an error in the upstream back-end service, testing that my view-cache is returning an error when back-end is returning an error. 

The Istio config looks like this
```yaml
 
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: back-end
spec:
  hosts:
    - back-end
  http:
    - fault:
        abort:
          httpStatus: 500
          percent: 100
      match:
        - uri:
            prefix: "/data"
      route:
        - destination:
            host: back-end


```


A 100% of the time this back-end service will return an http 500. 


Errata: In the test you can see we apply the Istio config from within the test 
```javascript
await exec('kubectl apply -f ./integration-tests/back-end-gateway-with-fault-injected.yaml')
``` 
This requires kubectl to be on the path. When you are executing a script using exec you might not have a logged in shell path, meaning running locally you might have to create a link in /bin/
```bash

sudo link  ~/google-cloud-sdk/bin/kubectl /bin/kubectl

```
 
 
Istio supports not only error but also slowdowns.  For example
```yaml

fault:
  delay:
    percentage:
      value: 100.0
    fixedDelay: 7s
```

will slow down all requests to the service and put a fixed delay of 7 seconds on them.

### Task 1 - Alter the view-cache service to not break down when the upstream back-end service is delayed (timeour gracefully) or returns an error  - write a test to prove this using the exmaple above as a start. You can find [a test scaffold in](../services/view-cache/integration-tests) and the test runner is configured as `npm run integration` in the   [view cache service directory](../services/view-cache)
### Task 2 - Run a performance test when 30% of requests are slowed down by 2 seconds and 10% of requests return an error  
### Task 3 - Configure a circuit breaker in Istio, break some upstream service all while running a load test




## Concluding Lab 6 

You are now finished with Lab 6, in this lab we learned
* How you can simulate network conditions in your load test on infrastructure level using Istio
* That enterprise level distributed computing requires you to be able to test against all sorts of failures 
