const request = require('request-promise-native')
const util = require('util');
const exec = util.promisify(require('child_process').exec);

describe('view cache service',  () => {

    test('should load resources from view-cache ', async () => {
        await exec('kubectl apply -f ./integration-tests/back-end-gateway-with-no-fault-injected.yaml')
        const result = JSON.parse(await request({timeout:1000, uri: 'http://35.184.138.147/get-data'}))
        expect(result).toBeInstanceOf(Array)
        expect(result.length).toBe(4)
    })

    //
    test('should load empty object if back end service is unreachable', async (done) => {
        const { stdout, stderr } =  await exec('echo $PATH')

        await exec('kubectl apply -f ./integration-tests/back-end-gateway-with-fault-injected.yaml')
        const promise = request({timeout:1000, uri:'http://35.184.138.147/get-data'})
        promise.then(function(value) {

            expect(false).toBeTruthy()
            done()
        }).catch(function(value) {
            expect(true).toBeTruthy()
            done()
            // expected output: "foo"
        })

    })
})

afterEach(async () => {
    await exec('kubectl apply -f ../back-end/back-end-gateway.yaml')
});
