# Lab 4 - Creating Custom Dashboards

At this point you have created a kubernetes cluster and were able to deploy your services onto it. Your app is running and  can start serving requests from customers. Everything looks fine, but wait! In order to bring this as a production-ready application, you must know the health of your services, you need some key performance indicators to understand their state.

In this lab you are going to build some dashboards using grafana and prometheus (which you may have guessed , come with istio for free). In order to do that, we are going to use some metrics called [" the four Golden Signals" ](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/#xref_monitoring_golden-signals)
These have become a industry  standard when designing monitoring systems for microservices and you should try to focus on them as an starting point.

The four metrics are:
- Latency
- Traffic
- Errors
- Saturation


## Part 1 - Accessing grafana
When you applied dashboard.yaml in lab 1, you exposed a set of telemetry tools using an istio ingressgateway. As you can see there, grafana is running on port 15031, so you can access it by opening in your web browser http://your.ingress.public-ip:15031

Get your ingress IP running:

`kubectl get svc -n istio-system | grep ingressgateway`


## Part 2 - Importing a dashboard
